const { Router } = require('express');
const authController = require('../controllers/aythController');
const { requireAuth, checkUser } = require('../middleware/authMiddleware');

const router = Router();

router.get('/api/auth/register', authController.register_get);
router.post('/api/auth/register', authController.register_post);

router.get('/api/auth/login', authController.login_get);
router.post('/api/auth/login', authController.login_post);
router.post('/api/auth/forgot_password', authController.forgotPass_post);
router.get('/api/auth/logout', authController.logout_get);

module.exports = router;
