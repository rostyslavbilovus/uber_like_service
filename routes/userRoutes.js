const { Router } = require('express');
const userControler = require('../controllers/userControler');
const { requireAuth, checkUser } = require('../middleware/authMiddleware');

const router = Router();

router.get('/api/users/me', requireAuth, userControler.profile_get);
router.delete('/api/users/me', checkUser, userControler.profile_delete);
router.patch('/api/users/me/password', userControler.profile_patch);

module.exports = router;
