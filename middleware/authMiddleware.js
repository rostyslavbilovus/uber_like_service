const JWT= require('jsonwebtoken');
const User = require('../models/User');

const requireAuth = (req, res, next) => {
  const token = req.cookies.JWT;

  // check json web token exists & is verified
  if (token) {
    JWT.verify(token, 'Rostyslav has a secret', (err, decodedToken) => {
      if (err) {
        console.log(err.message);
        res.redirect('/api/auth/login');
      } else {
        console.log(decodedToken);
        next();
      }
    });
  } else {
    res.redirect('/api/auth/login');
  }
};

// check current user
const checkUser = (req, res, next) => {
  const token = req.cookies.JWT;
  if (token) {
    JWT.verify(token, 'Rostyslav has a secret', async (err, decodedToken) => {
      if (err) {
        res.locals.user = null;
        next();
      } else {
        let user = await User.findById(decodedToken.id);
        res.locals.user = user;
        next();
      }
    });
  } else {
    res.locals.user = null;
    next();
  }
};

module.exports = { requireAuth, checkUser };