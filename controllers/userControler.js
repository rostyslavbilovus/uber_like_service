const User = require('../models/User');
const JWT = require('jsonwebtoken');


//============= Profile ==============

module.exports.profile_get = async (req, res, next) => {
  const token = req.cookies.JWT;
  if (token) {
    JWT.verify(token, 'Rostyslav has a secret', async (err, decodedToken) => {
      if (err) {
        res.locals.user = null;
        next();
      } else {
        let user = await User.findById(decodedToken.id);
          res.locals.user = user;

          res
              .status(200)
              .json({ _id: user._id, role: user.role, email: user.email, createdDate: user.createdAt });

        //   res.send('profile');    
        next();
      }
    });
  } else {
    res.locals.user = null;
    next();
  }
};

module.exports.profile_delete = async (req, res, next) => {
    
  const token = req.cookies.JWT;
  if (token) {
    JWT.verify(token, 'Rostyslav has a secret', async (err, decodedToken) => {
      if (err) {
        res.locals.user = null;
        next();
      } else {
          let user = await User.findOneAndDelete(decodedToken.id);
          res.locals.user = user;
           res
            .status(200)
               .json({ message: `Success! Profile ${user.email} was deleted` })
        next();
      }
    });
  } else {
    res.locals.user = null;
    next();
  }
};

module.exports.profile_patch = (req, res, next) => {
          
  const token = req.cookies.JWT;
  if (token) {
    JWT.verify(token, 'Rostyslav has a secret', async (err, decodedToken) => {
        if (err) {
            res.locals.user = null;
            next();
        } else {
          const user = await User.findById(decodedToken.id);
          
          res.locals.user = user;

          const oldPassword = req.body.oldPassword;
          const newPassword = req.body.newPassword;

          const auth = await bcrypt.compare(oldPassword, user.password);
        
          if (auth) {
            const psw = await bcrypt.hash(newPassword, await bcrypt.genSalt());

            await User.findOneAndUpdate(
              { _id: user._id },
              {
                $set: {
                  password: psw
                }
              }
            );
            
            res.status(200)
              .json({ message: `Success! Password changed`, user: user.email, password: user.password });
      
              return user;
          }

          throw Error('Incorrect old password');
      }
      
      next();
           
    });
  } else {
    res.locals.user = null;
    next();
  }
    // res.render('profile');
};
