const User = require('../models/User');
const JWT = require('jsonwebtoken');

// const Notes = require('../models/notes');
// const bcrypt = require('bcrypt');
// const mongoose = require('mongoose');


const handleErrors = (err) => {
    console.log(err.message, err.code);
    
  let errors = { email: '', password: '' };

  // incorrect email
  if (err.message === 'Incorrect email') {
    errors.email = 'That email is not registered';
  }

  // incorrect password
  if (err.message === 'Incorrect password') {
    errors.password = 'That password is incorrect';
  }

  // duplicate email username
  if (err.code === 11000) {
    errors.email = 'that user is already registered';
    return errors;
    }

  // validation errors
  if (err.message.includes('User validation failed')) {
    // console.log(err);
      Object.values(err.errors).forEach((error) => {
          errors[error.properties.path] = error.properties.message;
      });
  }

  return errors;
}

const maxAge = Math.floor(Date.now() / 1000) + (60 * 60);

const createToken = (id) => {
    return JWT.sign({ id }, 'Rostyslav has a secret', {
        expiresIn: maxAge
    })
}
//============== Register & login =====================

module.exports.register_get = (req, res) => {
    res.render('signup');
};

module.exports.login_get = (req, res) => {
    res.render('login');
};

module.exports.register_post = async (req, res) => {
    const {email, password, role} = req.body
    
    try {
        
        const user = await User.create({ email, password, role});
        const token = createToken(user._id);
        res.cookie('JWT', token, { httpOnly: true, maxAge: maxAge * 1000}, user);
      res.status(200)
        .json({
            "message": "Profile created successfully", user
              });
    }
    catch (err) {
        const errors = handleErrors(err);
        res.status(400).json({ errors });
    }
};

module.exports.login_post = async (req, res) => {
    const {email, password} = req.body

    try {
        const user = await User.login(email, password);
        const token = createToken(user._id);
        res.cookie('JWT', token, { httpOnly: true, maxAge: maxAge * 1000});
        res.status(200).json({ message: 'Success', user_id: user._id, JWT: req.cookies.JWT})
    } catch (err) {

        const errors = handleErrors(err);
        res.status(400).json({ errors });
    }
};

module.exports.forgotPass_post = async (req, res) => {
  const { email } = req.body;
  const user = await User.findOne({ email });

  if (user) {
    res.status(200).json({
      "message": "New password sent to your email address"
    })
  } else {
     res.status(400).json({"message": "That email is not registered" });
  }

}

module.exports.logout_get = (req, res) => {
    res.cookie('JWT', '', { maxAge: 1 });
    res.redirect('/')
}


// //============= notes ==============

// module.exports.notes_get = (req, res, next) => {
//     const token = req.cookies.JWT;
//   if (token) {
//     JWT.verify(token, 'Rostyslav has a secret', async (err, decodedToken) => {
//       if (err) {
//         res.locals.user = null;
//         next();
//       } else {
//         let user = await User.findById(decodedToken.id);
//         res.locals.user = user;
                
//         if (await Notes.findOne({ userId: user._id })) {
//           const data = await Notes.findOne({ userId: user._id });
//           data.notes.userId = user._id; 
//           data.count = data.notes.length;
//           data.notes.createdDate = data.notes.createdAt;
//           res
//             .status(200)
//             .json({message: 'Success!', data});

//         } else {
//           res
//             .status(400)
//             .json({message: `User ${user.username} has no notes`});

//         }
        
//         next();
//       }
//     });
//   } else {
//     res.locals.user = null;
//     next();
//   }
// };

// module.exports.notes_post = async (req, res, next) => {
//   const text = await req.body.text;

//    const token = req.cookies.JWT;
//   if (token) {
//     JWT.verify(token, 'Rostyslav has a secret', async (err, decodedToken) => {
//       if (err) {
//         res.locals.user = null;
//         next();
//       } else {
//         let user = await User.findById(decodedToken.id);

//         if (user) {
//           if (!await Notes.findOne({userId: user._id})) {
//             userNotes = await Notes.create({
//               userId: user._id,
//               notes: []
//             });
//           }

//           await Notes.findOneAndUpdate(
//             { userId: user._id },
//             { $push: {notes: {text, userId: user._id}} }
//           );

//           const testData = await Notes.findOne({ userId: user._id });

//           res.status(200)
//             .json({ message: `Success! User ${user.username} added a note`, ...testData.toJSON()});
          
//           next();
//         } else {
//           res.status(400)
//             .json({ message: `User not found` });
          
//           next();
//         }
//       }
//     })
//   } else {
//     res.locals.user = null;
//     next();
//   }
// };

// module.exports.notes_get_id = (req, res, next) => {
//   const token = req.cookies.JWT;
//   if (token) {
//     JWT.verify(token, 'Rostyslav has a secret', async (err, decodedToken) => {
//       if (err) {
//         res.locals.user = null;

//         next();
//       } else {
//         let user = await User.findById(decodedToken.id);

//         let id = req.params.id;

//         res.locals.user = user;

//         const userNote = await Notes.findOne(
//          { userId: user._id },
//           {
//             notes: {
//               $elemMatch: {
//                 _id: mongoose.Types.ObjectId(id)
//               }
//             }
//           }
//         );

//         const noteById = userNote?.notes.length ? userNote.notes[0] : null;
        
//         console.log('notes=== ', noteById );
                
//         if (noteById) {
//           res
//             .status(200)
//             .json({
//               message: 'Success!', noteById });

//         } else {
//           res
//             .status(400)
//             .json({message: `${user.username}, Something went wrong please try again`});

//         }
        
//         next();
//       }
//     });
//   } else {
//     res.locals.user = null;
//     next();
//   }
// };

// module.exports.notes_put_id = (req, res, next) => {
//   const token = req.cookies.JWT;
//   if (token) {
//     JWT.verify(token, 'Rostyslav has a secret', async (err, decodedToken) => {
//       if (err) {
//         res.locals.user = null;

//         next();
//       } else {
//         let user = await User.findById(decodedToken.id);

//         let id = req.params.id;

//         res.locals.user = user;

//         const userNote = await Notes.updateOne(
//           {
//             notes: {
//               $elemMatch: {
//                 _id: mongoose.Types.ObjectId(id)
//               }
//             }
//           }, {$set: {
//             'notes.$.text': req.body.text,
//             }}
//         );

//         const updatedNote = await Notes.findOne(
//          { userId: user._id },
//           {
//             notes: {
//               $elemMatch: {
//                 _id: mongoose.Types.ObjectId(id)
//               }
//             }
//           }
//         );

//         const noteById = updatedNote?.notes.length ? updatedNote.notes[0] : null;
                        
//         if (userNote) {
//           res
//             .status(200)
//             .json({message: 'Success! The note has been updated', noteById});

//         } else {
//           res
//             .status(400)
//             .json({message: `${user.username}, Something went wrong please try again`});
//         }
        
//         next();
//       }
//     });
//   } else {
//     res.locals.user = null;
//     next();
//   }
// };

// module.exports.notes_patch_id = (req, res, next) => {
//   const token = req.cookies.JWT;
//   if (token) {
//     JWT.verify(token, 'Rostyslav has a secret', async (err, decodedToken) => {
//       if (err) {
//         res.locals.user = null;

//         next();
//       } else {
//         let user = await User.findById(decodedToken.id);

//         let id = req.params.id;

//         res.locals.user = user;

//         const checkedNote = await Notes.findOne(
//          { userId: user._id },
//           {
//             notes: {
//               $elemMatch: {
//                 _id: mongoose.Types.ObjectId(id)
//               }
//             }
//           }
//         );

//         const checkCondition = checkedNote?.notes.length ? checkedNote.notes[0] : null;

//         const userNote = await Notes.updateOne(
//           {
//             notes: {
//               $elemMatch: {
//                 _id: mongoose.Types.ObjectId(id)               
//               }
//             }
//           }, {$set: {
//             'notes.$.completed': !checkCondition.completed
//             }}
//         );

//         const updatedNote = await Notes.findOne(
//          { userId: user._id },
//           {
//             notes: {
//               $elemMatch: {
//                 _id: mongoose.Types.ObjectId(id)
//               }
//             }
//           }
//         );

//         const noteById = updatedNote?.notes.length ? updatedNote.notes[0] : null;
                       
//         if (userNote) {
//           res
//             .status(200)
//             .json({message: 'Success! The note has been Checked/unchecked', noteById});

//         } else {
//           res
//             .status(400)
//             .json({message: `${user.username}, Something went wrong please try again`});
//         }
        
//         next();
//       }
//     });
//   } else {
//     res.locals.user = null;
//     next();
//   }
// };


// module.exports.notes_delete_id = (req, res, next) => {
//   const token = req.cookies.JWT;
//   if (token) {
//     JWT.verify(token, 'Rostyslav has a secret', async (err, decodedToken) => {
//       if (err) {
//         res.locals.user = null;

//         next();
//       } else {
//         let user = await User.findById(decodedToken.id);

//         let id = req.params.id;

//         res.locals.user = user;

//         const userNote = await Notes.updateOne( 
//           {
//             $pull: {
//               notes: {
//                 _id: mongoose.Types.ObjectId(id)
//               }
//             }
//           }
          
//         );
                        
//         if (userNote) {
//           res
//             .status(200)
//             .json({message: 'Success! The note has been deleted'});

//         } else {
//           res
//             .status(400)
//             .json({message: `${user.username}, Something went wrong please try again`});
//         }
        
//         next();
//       }
//     });
//   } else {
//     res.locals.user = null;
//     next();
//   }
// };