const express = require('express');
const path = require('path');
const morgan = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const authRoutes = require('./routes/authRoutes');
const userRoutes = require('./routes/userRoutes');
const cookieParser = require('cookie-parser');
const { checkUser } = require('./middleware/authMiddleware');


const app = express();

const PORT = 8080;

const createPath = (page) => path.resolve(__dirname, 'views', `${page}.ejs`);

// middleware
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json())
app.use(express.json());
app.use(cookieParser());
app.use(express.static('public'));
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
app.use(authRoutes);

// view engine
app.set('view engine', 'ejs');

// database connection
const dataBase = 'mongodb+srv://node_admin:12345node@cluster0.h5lbl.mongodb.net/Uber?retryWrites=true&w=majority';

mongoose
    .connect(dataBase)
    .then(() => {
        app.listen(PORT, (error) => {
            error ? console.log(error) : console.log(`Listening port ${PORT}`);
        });
        console.log('Database Connected Successfully')
    })
    .catch((error) => { console.log(error) });

//routes
//============ home page=============
app.get('*', checkUser)

app.get('/', (req, res) => {
    
    res
        .status(200)
        .render(createPath('index'));
});

app.use(authRoutes);
app.use(userRoutes);
